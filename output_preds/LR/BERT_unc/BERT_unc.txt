Nb train is : 2604
STD DEV : 1.845851768154745
MEAN : 74.9725
____REPORT____
              precision    recall  f1-score   support

           0       0.67      0.82      0.74       145
           1       0.77      0.59      0.67       145

    accuracy                           0.71       290
   macro avg       0.72      0.71      0.70       290
weighted avg       0.72      0.71      0.70       290

Save output pred to BINARYDISCREL_out.txt
Saved
Nb train is : 1288
STD DEV : 1.4303736399976041
MEAN : 59.802499999999995
____REPORT____
              precision    recall  f1-score   support

           0       0.78      0.81      0.79        36
           1       0.48      0.67      0.56        36
           2       0.73      0.53      0.61        36
           3       0.55      0.47      0.51        36

    accuracy                           0.62       144
   macro avg       0.64      0.62      0.62       144
weighted avg       0.64      0.62      0.62       144

Save output pred to RELSSEM_out.txt
Saved
Nb train is : 6140
STD DEV : 0.16926310879810746
MEAN : 90.69999999999999
____REPORT____
              precision    recall  f1-score   support

           0       0.94      0.86      0.90       341
           1       0.87      0.94      0.91       341

    accuracy                           0.90       682
   macro avg       0.90      0.90      0.90       682
weighted avg       0.90      0.90      0.90       682

Save output pred to SHIFTEDUNITS_out.txt
Saved
Nb train is : 6140
STD DEV : 0.13009611831257842
MEAN : 86.22500000000001
____REPORT____
              precision    recall  f1-score   support

           0       0.80      0.90      0.85       341
           1       0.88      0.77      0.82       341

    accuracy                           0.84       682
   macro avg       0.84      0.84      0.84       682
weighted avg       0.84      0.84      0.84       682

Save output pred to SCRAMBLEDUNITS_out.txt
Saved
{'DetectRelationsBin': {'devacc': 76.15, 'acc': 70.69, 'ndev': 288, 'ntest': 290}, 'RelsSem': {'devacc': 61.03, 'acc': 61.81, 'ndev': 144, 'ntest': 144}, 'ShiftedUnits': {'devacc': 90.99, 'acc': 90.18, 'ndev': 682, 'ntest': 682}, 'ScrambledUnits': {'devacc': 86.43, 'acc': 83.58, 'ndev': 682, 'ntest': 682}}
