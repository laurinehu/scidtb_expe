Nb train is : 2604
STD DEV : 0.8519976525789251
MEAN : 72.19
____REPORT____
              precision    recall  f1-score   support

           0       0.76      0.74      0.75       145
           1       0.75      0.76      0.75       145

    accuracy                           0.75       290
   macro avg       0.75      0.75      0.75       290
weighted avg       0.75      0.75      0.75       290

Save output pred to BINARYDISCREL_out.txt
Saved
Nb train is : 1288
STD DEV : 0.7514818693754346
MEAN : 53.705
____REPORT____
              precision    recall  f1-score   support

           0       0.86      0.50      0.63        36
           1       1.00      0.03      0.05        36
           2       0.37      0.92      0.52        36
           3       0.53      0.47      0.50        36

    accuracy                           0.48       144
   macro avg       0.69      0.48      0.43       144
weighted avg       0.69      0.48      0.43       144

Save output pred to RELSSEM_out.txt
Saved
Nb train is : 6140
STD DEV : 0.5651714341684337
MEAN : 79.0875
____REPORT____
              precision    recall  f1-score   support

           0       0.78      0.80      0.79       341
           1       0.80      0.77      0.79       341

    accuracy                           0.79       682
   macro avg       0.79      0.79      0.79       682
weighted avg       0.79      0.79      0.79       682

Save output pred to SHIFTEDUNITS_out.txt
Saved
Nb train is : 6140
STD DEV : 0.2204965986132195
MEAN : 76.07749999999999
____REPORT____
              precision    recall  f1-score   support

           0       0.79      0.74      0.77       341
           1       0.76      0.80      0.78       341

    accuracy                           0.77       682
   macro avg       0.77      0.77      0.77       682
weighted avg       0.77      0.77      0.77       682

Save output pred to SCRAMBLEDUNITS_out.txt
Saved
{'DetectRelationsBin': {'devacc': 73.66, 'acc': 75.17, 'ndev': 288, 'ntest': 290}, 'RelsSem': {'devacc': 54.58, 'acc': 47.92, 'ndev': 144, 'ntest': 144}, 'ShiftedUnits': {'devacc': 79.56, 'acc': 78.89, 'ndev': 682, 'ntest': 682}, 'ScrambledUnits': {'devacc': 76.32, 'acc': 77.13, 'ndev': 682, 'ntest': 682}}
