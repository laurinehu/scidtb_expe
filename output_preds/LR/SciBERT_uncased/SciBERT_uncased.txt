Nb train is : 2604
STD DEV : 1.0116168988307783
MEAN : 77.97749999999999
____REPORT____
              precision    recall  f1-score   support

           0       0.74      0.82      0.78       145
           1       0.80      0.71      0.75       145

    accuracy                           0.77       290
   macro avg       0.77      0.77      0.76       290
weighted avg       0.77      0.77      0.76       290

Save output pred to BINARYDISCREL_out.txt
Saved
Nb train is : 1288
STD DEV : 1.6508236580567903
MEAN : 60.167500000000004
____REPORT____
              precision    recall  f1-score   support

           0       0.96      0.61      0.75        36
           1       1.00      0.14      0.24        36
           2       0.43      0.53      0.48        36
           3       0.38      0.75      0.50        36

    accuracy                           0.51       144
   macro avg       0.69      0.51      0.49       144
weighted avg       0.69      0.51      0.49       144

Save output pred to RELSSEM_out.txt
Saved
Nb train is : 6140
STD DEV : 0.21714050750608285
MEAN : 92.13
____REPORT____
              precision    recall  f1-score   support

           0       0.91      0.94      0.92       341
           1       0.94      0.91      0.92       341

    accuracy                           0.92       682
   macro avg       0.92      0.92      0.92       682
weighted avg       0.92      0.92      0.92       682

Save output pred to SHIFTEDUNITS_out.txt
Saved
Nb train is : 6140
STD DEV : 0.41829266070539756
MEAN : 87.7175
____REPORT____
              precision    recall  f1-score   support

           0       0.85      0.85      0.85       341
           1       0.85      0.85      0.85       341

    accuracy                           0.85       682
   macro avg       0.85      0.85      0.85       682
weighted avg       0.85      0.85      0.85       682

Save output pred to SCRAMBLEDUNITS_out.txt
Saved
{'DetectRelationsBin': {'devacc': 78.84, 'acc': 76.55, 'ndev': 288, 'ntest': 290}, 'RelsSem': {'devacc': 61.95, 'acc': 50.69, 'ndev': 144, 'ntest': 144}, 'ShiftedUnits': {'devacc': 92.49, 'acc': 92.23, 'ndev': 682, 'ntest': 682}, 'ScrambledUnits': {'devacc': 88.42, 'acc': 84.9, 'ndev': 682, 'ntest': 682}}
